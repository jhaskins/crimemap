import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('map', { path: '/' });
  this.route('neighborhoods', { path: '/neighborhoods' });
  this.route('neighborhood', function(){
      this.route('view', { path: '/:neighborhood' });
    });
  });

export default Router;
