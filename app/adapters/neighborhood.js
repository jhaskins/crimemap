import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  coalesceFindRequests: true,
  buildURL: function(root, suffix, record) {
    var url = 'https://data.detroitmi.gov/resource/w48v-ra3x.json';
    return url;
  },
});
