import DS from 'ember-data';

export default DS.Model.extend({
  caseid: DS.attr('number'),
  category: DS.attr(),
  description: DS.attr(),
  address: DS.attr(),
  date: DS.attr(),
  incidentdate: DS.attr(),
  lat: DS.attr('number'),
  lng: DS.attr('number'),
  hour: DS.attr('number'),
  council: DS.attr(),
  precinct: DS.attr('number')
});
