import DS from 'ember-data';

export default DS.Model.extend({
  offence_class: DS.attr('number'),
  category: DS.attr(),
  icon: DS.attr(),

});
