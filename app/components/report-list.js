import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',

  didRender() {
    let height = Ember.$(window).height() - Ember.$('.map').offset().top - Ember.$('.map').height() - 40;
    Ember.$('.report').css('height', height);
  }
});
