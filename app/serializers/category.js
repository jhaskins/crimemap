import RESTAPISerializer from 'ember-data/serializers/rest';

export default RESTAPISerializer.extend({
  primaryKey: 'offence_class',
  normalizeResponse: function(store, primaryModelClass, payload, id, requestType) {
    console.dir(payload);
    var normalizedPayload = {categories: []};

    payload.categories.forEach(function(item) {
      var category = {
        id: item.class,
        offence_class: item.class,
        category: item.desc,
//        icon: item.icon,
      };
      normalizedPayload.categories.push(category);
    });
    console.dir(normalizedPayload);
    return this._super(store, primaryModelClass, normalizedPayload, id, requestType);
  }
});
