import RESTAPISerializer from 'ember-data/serializers/rest';

export default RESTAPISerializer.extend({
  primaryKey: 'caseid',
  normalizeResponse: function(store, primaryModelClass, payload, id, requestType) {
    //console.dir(payload);
    var self = this;
    var normalizedPayload = {reports: []};

    payload.forEach(function(item) {

      var date = moment(item.incidentdate);
      var address = self.parseAddress(item.address);


      var report = {
        id: item.caseid,
        caseid: item.caseid,
        category: item.category,
        description: item.offensedescription,
        address: address,
        lat: item.lat,
        lng: item.lon,
        date: date,
        incidentdate: date.format('LL'),
        hour: item.hour,
      };
      normalizedPayload.reports.push(report);
    });
    return this._super(store, primaryModelClass, normalizedPayload, id, requestType);
  },

  parseAddress: function(addr){
    var r = {
      num: null,
      street: null,
      type: null
    };
    var addressComponents = addr.split(/\s/g);
    r.num = Number.parseInt(addressComponents.shift());
    if(r.num == 0) {
      r.num  = '';
    }
    else {
      r.num = r.num + ' BLOCK ';
    }
    r.street = addressComponents.join(' ');
    return r.num + r.street;
  }
});
