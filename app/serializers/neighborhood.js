import RESTAPISerializer from 'ember-data/serializers/rest';

export default RESTAPISerializer.extend({
  normalizeResponse: function(store, primaryModelClass, payload, id, requestType) {
    var normalizedPayload = {neighborhoods: []};

    payload.forEach(function(item) {
      var url_name = item.name.toLowerCase().replace(/\s+/g, '-').replace(/[^a-zA-Z0-9\-]/g, "");
      var neighborhood = {
        id: url_name,
        name: item.name,
        geom: item.the_geom,
      };
      normalizedPayload.neighborhoods.push(neighborhood);
    });
    return this._super(store, primaryModelClass, normalizedPayload, id, requestType);
  }
});
