import Ember from 'ember';

export default Ember.Route.extend({
  init: function () {
    this._super();
    // Get the list of categories.
    this.store.findAll('category');
  },
});
