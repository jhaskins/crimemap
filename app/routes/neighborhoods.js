import Ember from 'ember';

export default Ember.Route.extend({
  model: function(parameters) {
    console.dir(parameters);
    return this.store.query('neighborhood', {'$select': 'name,the_geom'});
  }
});
