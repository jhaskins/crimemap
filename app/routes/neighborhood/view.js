import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params) {
    var neighborhood = this.store.peekRecord('neighborhood', params.neighborhood);
    if(neighborhood == null) {
      var self = this;
      var like_query = params.neighborhood;
      neighborhood = this.store.query('neighborhood', {'$select': 'name,the_geom', '$where':'name like' + like_query}).then(function(neighborhoods) {

      });
    }

    neighborhood.then(function(self) {
      var geom = self.get('geom');
      var polygon = '';
      geom.coordinates[0][0].forEach(function(item) {
        polygon = polygon + item[1] + ' ' + item[0] + ',';
      });
      return this.store.query('report', {'$where': 'within_polygon(location, \'' + geom.type + ' (((' + polygon + ')))\')'});
    });


  }
});
