import Ember from 'ember';

export default Ember.Controller.extend({
  lat: 42.3484817,
  lng: -83.2578705,
  zoom: 15,
  bounds: '',
  center: '',
  reports: null,
  message: null,
  startDate: null,
  endDate : null,
  count: 0,
  location: '',

  init: function() {
    var startDate = new Date();
    startDate.setDate(startDate.getDate()-30);
    var endDate = new Date();

    this.set('startDate', startDate.getMonth()+1 + '/' + startDate.getDate() + '/' + startDate.getFullYear());
    this.set('endDate', endDate.getMonth()+1 + '/' + endDate.getDate() + '/' + endDate.getFullYear());
  },


  actions: {
    updateCenter(e) {
      var bbox = e.target.getBounds();
      this.set('bounds', bbox);
      this.updateMap();
    },

    // Geocode an input value and recenter the map on that point.
    recenterMap() {
      // @TODO: geocode address using http://geocoding.geo.census.gov/geocoder/Geocoding_Services_API.html
      // @TODO: recenter map on resulting coords.
    }

  },

  // Query the api and update the map.
  updateMap() {
    var bbox = this.get('bounds');
    var nw = bbox.getNorthWest();
    var se = bbox.getSouthEast();

    var self = this;
    this.set('message', null);

    var startDate = moment(this.get('startDate'));
    var endDate = moment(this.get('endDate'));

    var isoDateStart = startDate.format('YYYY-MM-DD');
    var isoDateEnd = endDate.format('YYYY-MM-DD');

    this.store.query('report', {'$where': 'within_box(location, ' + nw.lat + ',' + nw.lng + ',' + se.lat + ',' + se.lng +') AND incidentdate between \'' + isoDateStart +'\' and \'' + isoDateEnd + '\''}).then(
      function(reports) {
        var length = reports.get('length');
        if(length >= 1000) {
          self.set('message', 'Unable to display all reports.  Please zoom in.');
        }
        self.set('reports', reports);
        self.set('count', reports.get('length'));
        return true;
      }
    );
  },

  // Add an observer to update the map when the start date changes.
  updateStartDate: Ember.observer('startDate', function() {
    var value = this.get('startDate');
    // @TODO: add some more validation of the date, combine with end date observer?
    if(typeof value !== 'undefined') {
      this.updateMap();
    }
  }),

  // Add an observer to update the map when the end date changes.
  updateEndDate: Ember.observer('endDate', function() {
    var value = this.get('endDate');
    if(typeof value !== 'undefined') {
      this.updateMap();
    }
  }),

});
